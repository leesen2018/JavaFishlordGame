package org.gpf.fishlord;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
/**
 * 渔网类：网的位置随着鼠标指针的移动而移动
 * @author gaopengfei
 * @date 2015-4-12 下午9:57:06
 */
class Net {
	BufferedImage netImage = null;
	int x = 0, y = 0, width, height;
	boolean show;					// 是否显示当前网对象，鼠标移出游戏界面时，渔网消失

	/**
	 * 构造器中加载渔网的背景图片，刚刚显示出游戏界面时是看不见渔网的
	 */
	public Net(){
		
		try {
			netImage = ImageIO.read(getClass().getResourceAsStream("/images/net.png"));
		} catch (IOException e) {
			System.out.println("渔网资源加载失败！");
			e.printStackTrace();
		}
		show = false;
		width = netImage.getWidth();
		height = netImage.getHeight();
	}
}